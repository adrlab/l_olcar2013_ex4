function dtheta = PI2(bsim_out,bR)
% D is the data structure of all roll outs, and R the cost matrix for these roll outs

n_dof = 3;
n_bfs = length(bsim_out(1).Controller.theta(:,1));
PI2_ave_mode = 3;

% Change of conventions
R = bR';
[n,n_reps] = size(R);

D = struct;
D.epsilone = zeros(n,n_dof);

D(1:n_reps) = D;
%%%%%%%%%%%
for cnt = 1:length(bsim_out)
    sim_out = bsim_out(cnt);
    T = sim_out.t;
    ntemp = size(T,2);
    X = sim_out.x;
    U = sim_out.u;
    U = [U U(:,ntemp)*ones(1,n-ntemp)];
    Psi = sim_out.Controller.BaseFnc(T,X);
    Psi = [Psi Psi(:,ntemp)*ones(1,n-ntemp)];
    theta = sim_out.Controller.theta;
    
    thetaTpsi = theta'*Psi;
    
    D(cnt).epsilon  = (U - thetaTpsi)';
    for cnt1 = 1:n_dof
        D(cnt).controller(cnt1).phi = Psi';
        D(cnt).controller(cnt1).theta = theta;
    end  
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for j=1:n_dof,
  for k=1:n_reps,
    
    
    % compute g'*eps in vector form
    gTeps = D(k).epsilon(:,j);
    
    
    % compute g'g
    gTg = sum(D(k).controller(j).phi.*D(k).controller(j).phi,2);
    
    % compute P*M*eps = g*g'*eps/(g'g) from previous results
    Meps = D(k).controller(j).phi.*((gTeps./(gTg + 1.e-10))*ones(1,n_bfs));
    theta_hat = D(k).controller(j).theta + Meps; %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            gTeps = eps;
            % compute g'g
            gTg = sum(D(k).controller(i).phi.*D(k).controller(i).phi,2);
            % compute P*M*eps = P*g*g'*eps/(g'g) from previous results
            Meps = D(k).controller(i).phi.*((gTeps./(gTg + 1.e-10))*ones(1,n_bfs));
            theta_hat = D(k).controller(i).theta + Meps;
            rp = rp + sum(RR*theta_hat.*theta_hat,2);

% compute the accumulate cost
S = rot90(rot90(cumsum(rot90(rot90(R)))));


% compute the exponentiated cost with the special trick to automatically
% adjust the lambda scaling parameter
maxS = max(S,[],2);
minS = min(S,[],2);

h = 10; % this is the scaling parameters in side of the exp() function (see README.pdf)
expS = exp(-h*(S - minS*ones(1,n_reps))./((maxS-minS)*ones(1,n_reps)));

% the probabilty of a trajectory
P = expS./(sum(expS,2)*ones(1,n_reps));

% compute the projected noise term. It is computationally more efficient to break this
% operation into inner product terms. 
PMeps = zeros(n_dof,n_reps,n,n_bfs);

for j=1:n_dof,
  for k=1:n_reps,
    
    
    % compute g'*eps in vector form
    gTeps = D(k).epsilon(:,j);
    
    
    % compute g'g
    gTg = sum(D(k).controller(j).phi.*D(k).controller(j).phi,2);
    
    % compute P*M*eps = P*g*g'*eps/(g'g) from previous results
    PMeps(j,k,:,:) = D(k).controller(j).phi.*((P(:,k).*gTeps./(gTg + 1.e-10))*ones(1,n_bfs));

  end
end

% compute the parameter update per time step
dtheta = squeeze(sum(PMeps,2));

% average updates over time
switch PI2_ave_mode
    case 1      % The uniform weighting
        W = ones(n,n_bfs);
    case 2      % The time weighting matrix
        m = D(1).duration/D(1).dt;
        N = (m:-1:1)';
        N = [N; ones(n-m,1)];
        W = (N*ones(1,n_bfs));
    case 3      % The time weighting matrix which takes the kernel activation into account
        % The time weighting matrix
        N = (n:-1:1)';
        % the final weighting vector takes the kernel activation into account
        W = (N*ones(1,n_bfs)) .* D(1).controller(1).phi;
end

% ... and normalize through time
W = W./(ones(n,1)*sum(W,1));

% compute the final parameter update for each DMP
dtheta = squeeze(sum(dtheta.*repmat(reshape(W,[1,n,n_bfs]),[n_dof 1 1]),2));
dtheta = dtheta';
